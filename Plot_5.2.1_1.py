import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import numpy as np
from scipy.stats import linregress
T_hot1=[296.8,297.3,297.7,298.30,299.70,302.00,303.70,305.20,306.20,307.00,307.70,308.20,308.60,308.90,309.10,309.20,309.30,309.40,309.50,309.50,309.60,309.60,309.60,309.70,310.20]
T_cold1=[297.4,297.4,297.5,297.50,297.60,297.90,298.20,298.20,298.30,298.40,298.50,298.30,298.30,298.50,298.50,298.60,298.60,298.50,298.50,298.40,298.40,298.50,298.60,299.00,299.30]
V_th1=[0.6,11.8,24.9,37.00,69.00,118.90,155.40,184.40,207.50,226.10,240.00,251.60,260.00,268.20,273.90,277.00,280.00,283.50,285.30,286.90,287.40,286.50,286.60,287.10,290.50,]
delta_T1=[T_hot1[i]-T_cold1[i] for i in range(len(T_hot1))]
T_hot2=[310.20,309.20,307.90,306.00,304.10,302.90,301.90,300.90,300.40,299.60,299.20,298.90,298.70,298.40,298.20,297.90,297.80,297.80]
T_cold2=[299.70,299.30,299.20,298.90,298.80,298.50,298.30,298.20,298.20,298.10,298.10,298.00,297.90,297.90,298.00,298.00,298.00,298.00]
V_th2=[276.60,251.00,219.70,170.00,133.90,106.00,88.80,70.80,61.00,44.10,35.50,32.00,26.20,22.00,15.40,10.10,8.20,6.40]
t_kühlen=[10.00,30.00,60.00,120.00,180.00,247.30,301.00,360.00,400.00,490.60,560.00,601.00,660.00,720.00,793.00,943.00,1068.00,1206.00]
delta_T2=[T_hot2[i]-T_cold2[i] for i in range(len(T_hot2))]
deltaT=delta_T1+delta_T2
V_th=V_th1+V_th2
x_error1=[np.sqrt((0.0015*T_cold1[i]+0.1)**2+(0.0015*T_hot1[i]+0.1)**2) for i in range(len(T_cold1))] 
x_error2=[np.sqrt((0.0015*T_cold2[i]+0.1)**2+(0.0015*T_hot2[i]+0.1)**2) for i in range(len(T_cold2))] 

xerr=x_error1+x_error2
print(xerr)
V_th= [i*10**-3 for i in V_th]
def fit_gerade(x,a):
    return(a*(x))
def kühl_funktion(x,a):
    l=sum(T_cold2[8:])/(len(T_cold2[8:]))
    return((l+(T_hot2[0]-l)*np.exp(-a*x)))    
def plot_seebeck():
    y_error=[i*0.0015+0.2*10**-3 for i in V_th] 
    parameter, covariance_matrix = curve_fit(fit_gerade,deltaT,V_th,sigma=y_error)
    a=parameter[0]
    f= np.sqrt(np.diag(covariance_matrix))
    b=f[0]

    
    x_data=np.linspace(0,12,200)
    y_data=[fit_gerade(i,a) for i in x_data]
    linreg=linregress(deltaT,V_th)
    #y_data1=[(linreg[0]-linreg[4])*i for i in x_data]
    #y_data2=[(linreg[0]+linreg[4])*i  for i in x_data]
    y1=[(a-b)*i for i in x_data]
    y2=[(a+b)*i for i in x_data]
    #yreg=[linreg[0]*i for i in x_data]
    print('The Seeback coefficient is:', a)
    print('The standard deviation error is:', b)
    with plt.style.context('bmh'):
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.set_title('V_th against \u0394T (peltier device)')
        plt.errorbar(deltaT, V_th,yerr= y_error,fmt='none',color='red',elinewidth=1,capsize=2)
        plt.plot(deltaT,V_th,'x',color='black',label='V_th - \u0394T measurement',markersize=2)
        #plt.plot(x_data, yreg, label='fit two')
        plt.plot(x_data,y_data, label='fit_curve', color='blue', linewidth=1)
        plt.plot(x_data,y1,linewidth=0.5, linestyle="--",color='r',label='error_straight' )
        plt.plot(x_data,y2,linewidth=0.5, linestyle="--" ,color='r',label='error_straight' )
        #plt.plot(x_data,y_data1,linewidth=0.5, linestyle="--",color='r',label='error_straight_1' )
        #plt.plot(x_data,y_data2,linewidth=0.5, linestyle="--" ,color='r',label='error_straight_2' )
        ax.legend()
        ax.set_xlabel('\u0394T in Kelvin')
        ax.set_ylabel('V_th in Volt')
        plt.show()
def plotten_kühlen():
    yerr1=[0.0015*T_cold2[i]+0.1 for i in range(len(T_cold2))]
    yerr2=[0.0015*T_hot2[i]+0.1 for i in range(len(T_hot2))]
    parameter, covariance_matrix = curve_fit(kühl_funktion,t_kühlen,T_hot2, sigma=yerr2)
    p=np.linspace(3,1250,500)
    a= parameter
    b= covariance_matrix[0]
    b=np.sqrt(b)
    y_data=[kühl_funktion(i,a[0]) for i in p]
    y_data1=[kühl_funktion(i,a[0]+b) for i in p]
    y_data2=[kühl_funktion(i,a[0]-b) for i in p]
    print(' kappa/m*c is: ',a[0])
    m=0.22
    c=897
    
    print(' kappa is: ',a[0]*m*c)
    print('The error is:',a[0]*m*c-(a+b)*m*c)
    with plt.style.context('bmh'):
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.set_title('')
        plt.plot(t_kühlen,T_cold2,marker='s',label='T cold')
        plt.plot(t_kühlen,T_hot2,'x' ,label='measurement', color='black' )
        plt.errorbar(t_kühlen,T_hot2,yerr=yerr2,fmt='none',color='red',elinewidth=1,capsize=2)
        plt.errorbar(t_kühlen,T_cold2,yerr=yerr1,fmt='none',color='red',elinewidth=1,capsize=2)
        plt.plot(p,y_data, label='fit',color='blue')
        plt.plot(p,y_data1,linestyle="--" ,label='error', color='red',linewidth=1)
        plt.plot(p,y_data2,linestyle="--" ,label='error',color='red',linewidth=1)
        ax.legend()
        ax.set_title('Cooldown process')
        ax.set_xlabel('Zeit in s')
        ax.set_ylabel('T in Kelvin')
        plt.show()
plotten_kühlen()