#%%
import matplotlib.pyplot as plt
from matplotlib.patches import ConnectionPatch as conpatch
import numpy as np
from scipy.optimize import curve_fit

def amp_error(amperage):
    errors = []
    for i in amperage:
        if i < 4:
            error = i * 0.005 + 0.005/10**(3)
            errors.append(error)
        elif i < 40:
            error = i * 0.005 + 0.02/10**(3)
            errors.append(error)
    return(errors)            

def ohm_error(ohm):
    errors = []
    for i in ohm:
        errors.append(i*0.004+0.2)
    return(errors)

def func_2(x, a, b):
    return((a * x + b))

def func_5(x, a, b):
    return ((x * 10.8 * a - x**2)/ b)

def func_8(x,a,b):
    return((a**2 * 10.8**2 * x)/(x**2+(2*x * b)+(b**2)))

# aquisition
save_name = "i_v_pairs_steady_state.csv"
f = open(save_name, "r")
data_points = f.readlines()
f.close()

data_points = [x.rstrip('\n') for x in data_points]
data_points = [[x for x in i.split('\t')] for i in data_points]
data_points = data_points[1:]

R_values = [float(x[0]) for x in data_points]
I_values = [float(x[1]) for x in data_points]
V_values = [float(x[2]) for x in data_points]

# get errors
I_errors = (amp_error(I_values))
V_errors = [(i * 0.0015 + 0.0002) for i in V_values]
R_errors = ohm_error(R_values)
# print(I_errors, '\n', V_errors)
x = np.array(V_values)


# 2. Plot 5.2.2 I over V
print('\t5.2.2.2')

popt, pcov = curve_fit(func_2, V_values, I_values)
perr = np.sqrt(np.diag(pcov))
print('popt: ', popt, '\npcov:\n', pcov, '\nsigma: ', perr)
x_large = np.linspace(0,310, num= 311)

with plt.style.context('bmh'):
    fig = plt.figure(figsize=(6,13))
    fig.subplots_adjust(left=0.1 , right=1 , bottom=0.1, top=1)

    # large plot visiuals
    sub1 = fig.add_subplot(2,1,1)
    sub1.grid(True)
    sub1.set_xlim(0, 310)
    sub1.set_ylim(-5,100)
    sub1.set_xlabel('V in mV')
    sub1.set_ylabel('I in mA')
    sub1.fill_between((230, 300), -5, 100, facecolor='green', alpha=0.2)

    # large plot data
    sub1.errorbar(V_values, I_values, I_errors,fmt=',', capsize=2, ecolor='r', label='measurements')
    sub1.plot(x_large, func_2(x_large, popt[0], popt[1]), label='fit')
    sub1.plot(x_large, func_2(x_large, popt[0] + perr[0], popt[1] + perr[1]), label='upper')
    sub1.plot(x_large, func_2(x_large, popt[0] - perr[0], popt[1] - perr[1]), label='lower')

    # zoomed plot visuals
    sub2 = fig.add_subplot(2,1,2)
    sub2.set_xlim(230, 300)
    sub2.set_ylim(-5,25)
    sub2.set_xlabel('V in mV')
    sub2.set_ylabel('I in mA')
    sub2.fill_between((230, 300), -5, 100, facecolor='green', alpha=0.2)

    # zoomed plot data
    sub2.errorbar(V_values, I_values, I_errors,fmt=',', capsize=2, ecolor='r', label='measurements')
    sub2.plot(x_large, func_2(x_large, popt[0], popt[1]), label='fit')
    sub2.plot(x_large, func_2(x_large, popt[0] + perr[0], popt[1] + perr[1]), label='upper')
    sub2.plot(x_large, func_2(x_large, popt[0] - perr[0], popt[1] - perr[1]), label='lower')

    # connection patches to connect sub1 and sub2
    con1 = conpatch(
        xyA = (230, 10), coordsA=sub1.transData,
        xyB = (230, 25), coordsB=sub2.transData, color = 'green', linewidth = 1)
    con2 = conpatch(
        xyA = (300, 10), coordsA=sub1.transData,
        xyB = (300, 25), coordsB=sub2.transData, color = 'green', linewidth = 1)

    fig.add_artist(con1)
    fig.add_artist(con2)

    sub1.legend() # because we only want one legend as it is identical in both plots
    sub2.legend()
    plt.show()
    plt.close()

# all in SI units
I_values = [float(x[1])/1000 for x in data_points]
V_values = [float(x[2])/1000 for x in data_points]
I_errors = (amp_error(I_values))
V_errors = [(i * 0.0015 + 0.0002) for i in V_values]
R_errors = ohm_error(R_values)

print('\n\t5.2.2.4')
# 4. plot 5.2.2 power plot P=I*V 
# a = S (Seebeck), b = R_i 
V_array = np.array(V_values)
I_array = np.array(I_values)
popt, pcov = curve_fit(func_5, V_array, I_array*V_array)
perr = np.sqrt(np.diag(pcov))
print('popt: ', popt, '\npcov:\n', pcov, '\nsigma: ', perr)

P_values = I_array*V_array

Delta_U = I_array * V_errors
Delta_I = I_errors * V_array

P_errors = [np.sqrt(Delta_I[i]**2+Delta_U[i]**2) for i in range (len(Delta_I))]


with plt.style.context('bmh'):
    plt.figure()
    plt.xlabel('U in V')
    plt.ylabel('P in W')
    # plt.plot(V_array, I_array*V_array, 'x')
    plt.errorbar(V_array, P_values, P_errors, fmt='x',capsize=2, ecolor='r', label='measurements')
    plt.plot(V_array, func_5(V_array, popt[0], popt[1]), 'r-', label='fit')
    plt.plot(V_array, func_5(V_array, popt[0] + perr[0], popt[1] + perr[1]), label='upper')
    plt.plot(V_array, func_5(V_array, popt[0] - perr[0], popt[1] - perr[1]), label='lower')

    plt.legend()
    plt.title('Power as a function of I and V')
    plt.savefig('P_over_I_times_V')
    plt.show()
    plt.close()


# 5.2.2.6 20% larger DeltaT
print('\n\t5.2.2.6')
# TODO: add variation of I 

large_V = V_array *1.2
# print(V_array, '\n\n', large_V)
popt_20, pcov = curve_fit(func_5, large_V, I_array*large_V)
perr = np.sqrt(np.diag(pcov))

space = np.linspace(0,0.360, num= 501)

with plt.style.context('bmh'):
    plt.figure()
    plt.xlabel('U in V')
    plt.ylabel('P in W')
    plt.xlim(0.230,0.360)
    plt.ylim(0,0.006)

    # 20% increased data
    plt.plot(large_V, I_array*large_V, 'x', label='maniuplated x')
    plt.plot(space, func_5(space, popt_20[0], popt_20[1]), 'b-', label='large')
    # original data
    plt.plot(V_array, I_array*V_array, 'x', label='x')
    plt.plot(V_array, func_5(V_array, popt[0], popt[1]), 'r-', label='original')

    plt.title('Power as a function of I and V')
    plt.savefig('P_over_I_times_V_20')
    plt.show()
    plt.close()



# 5.2.2.7 plot power over resistance 
print('\n\t5.2.2.7')
# Removal of last element aka infinite resistance element
V_vals = V_values[:-1]
V_vals = np.array([x**2 for x in V_vals])
R_values = np.array(R_values[:-1]) + 11

# print('I: ', I_array, '\n\nV: ', V_array)

P_values = I_array*V_array
P_values = P_values[:-1]

Delta_U = I_array[:-1] * V_errors[:-1]
Delta_I = I_errors[:-1] * V_vals

P_errors = [np.sqrt(Delta_I[i]**2+Delta_U[i]**2) for i in range (len(Delta_I))]

# print('\nDelta_U:\n', Delta_U,'\nDelta_i:\n', Delta_I ,'\nP_errors:\n',P_errors,'\n\n')
# print('I errors:\n', I_errors,'\nU errors:\n', V_errors,'\nR errors:\n', R_errors,'\n\n')

x = np.linspace(0,500, num= 501)
popt, pcov = curve_fit(func_8, R_values, P_values)
perr = np.sqrt(np.diag(pcov))
print('S, R_i: ',popt, '\npcov:\n', pcov, '\nsigma: ', perr)


with plt.style.context('bmh'):
    plt.figure(figsize=(9,8))
    plt.xlabel('R in Ohm')
    plt.ylabel('P in W')
    plt.errorbar(R_values, P_values, P_errors, fmt='x',capsize=2, ecolor='r', label='measurements')
    # plt.plot(R_values, P_values, 'x')
    plt.plot(x, func_8(x, popt[0], popt[1]), label='plot', linewidth=0.6)
    plt.plot(x, func_8(x, popt[0]+perr[0], popt[1]+perr[1]), label='upper', linewidth=0.5, linestyle='dashed')
    plt.plot(x, func_8(x, popt[0]-perr[0], popt[1]-perr[1]), label='lower', linewidth=0.5, linestyle='dashed')
    plt.legend()

    plt.grid(True)
    plt.savefig('P_over_R_total')
    plt.show()
    plt.close()


# %%
