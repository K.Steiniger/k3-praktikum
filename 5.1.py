import numpy as np
T_room=[294.4,295.6]
T_cold=77
deltaT=sum(T_room)/2-77
U_PB_Cu=0.4*10**-3
U_PB_C=-6*10**-3
U_Cu_C=	-6.30*10**-3
print(deltaT)
def fehlerformel(V,T1,T2,Tcold,deltaV,deltaT1,deltaT2):
    return(np.sqrt((deltaV/((T1+T2)/2-Tcold))**2+(V*deltaT1/(2*((T1+T2)/2-Tcold)**2))**2+(V*deltaT2/(2*((T1+T2)/2-Tcold)**2))**2))

S_PB_Cu=U_PB_Cu/deltaT
S_PB_C=U_PB_C/deltaT
S_Cu_C=U_Cu_C/deltaT
print('S_PB_Cu:',S_PB_Cu,'error:' ,fehlerformel(U_PB_Cu,T_room[0],T_room[1],T_cold,U_PB_Cu*0.15*10**-2+0.2*10**-4,T_room[0]*0.15*10**-2*+1,T_room[1]*0.15*10**-2*+1))
print('S_PB_C:',S_PB_C,'error:',fehlerformel(U_PB_C,T_room[0],T_room[1],T_cold,U_PB_C*0.15*10**-2+2*10**-4,T_room[0]*0.15*10**-2*+1,T_room[1]*0.15*10**-2*+1))
print('S_Cu_C:',S_Cu_C,'error:',fehlerformel(U_PB_Cu,T_room[0],T_room[1],T_cold,U_PB_Cu*0.15*10**-2+2*10**-4,T_room[0]*0.15*10**-2*+1,T_room[1]*0.15*10**-2*+1))

